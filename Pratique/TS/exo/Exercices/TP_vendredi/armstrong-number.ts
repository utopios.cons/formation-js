export function isArmstrongNumber(num: number): boolean {
    const exponent = `${num}`.length
    const sum = `${num}`
        .split('')
        .map(v => Math.pow(+v, exponent))
        .reduce((a, b) => a + b);
    return num === sum
  }