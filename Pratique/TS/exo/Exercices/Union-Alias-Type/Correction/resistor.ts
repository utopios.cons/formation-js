enum ResistanceValue {
    black = 0,
    brown = 1,
    red = 2,
    orange = 3,
    yellow = 4,
    green = 5,
    blue = 6,
    violet = 7,
    grey = 8,
    white = 9,
}
type Color = keyof typeof ResistanceValue;
let colo: Color[];
console.log(colo);

type Unit = 'ohms' | 'kiloohms';

function decodedResistorValue(colors: Color[]): string {
    const [colorOne, colorTwo, colorThree] = colors;
    let result: number = colorOne ? ResistanceValue[colorOne] : 0;
    let unit: Unit = 'ohms';
    if (colorTwo) {
        result = result * 10 + ResistanceValue[colorTwo];
    }
    if (colorThree) {
        result = result * 10 ** ResistanceValue[colorThree];
        unit = result > 1000 ? 'kiloohms' : unit;
        result = result > 1000 ? result / 1000 : result;
    }
    return `${result} ${unit}`;
}

let col: Color[] = ["orange", "orange", "blue"];

let resu = decodedResistorValue(col);

console.log(resu);