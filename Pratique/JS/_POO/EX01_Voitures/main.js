const resultOutput = document.querySelector('#result')

class Voiture {

    constructor(brand, model, speed) {
        this.brand = brand;
        this.model = model;
        this.speed = speed;
    }

    accelerate() {
        this.speed += 10;
        console.log(this.toString())
        return `<p>La voiture <b>${this.brand} ${this.model}</b> avance désormais à <b>${this.speed}km/h</b></p>`
    }

    brake() {
        this.speed -= 5;
        console.log(this.toString())
        return `<p>La voiture <b>${this.brand} ${this.model}</b> avance désormais à <b>${this.speed}km/h</b></p>`
    }

    toString() {
        return `Voiture : ${this.brand} ${this.model} va à ${this.speed}km/h.`;
    }
}

document.querySelector("#result").innerHTML += `<h2>Course de Voiture</h2><hr>`

let bmw = new Voiture("BMW", "Serie 1", 80);
let mercedes = new Voiture("Mercedes", "GLB", 100);

console.log(bmw.toString())
console.log(mercedes.toString())

resultOutput.innerHTML += bmw.accelerate()
resultOutput.innerHTML += mercedes.brake()
resultOutput.innerHTML += bmw.accelerate()
resultOutput.innerHTML += bmw.accelerate()
resultOutput.innerHTML += mercedes.brake()
resultOutput.innerHTML += mercedes.accelerate()
resultOutput.innerHTML += mercedes.accelerate()