const resultOutput = document.querySelector("#result")

class Voiture {

    constructor(brand, model, speed) {
        this.brand = brand;
        this.model = model;
        this.speed = speed;
    }

    get speedUS() {
        return this.speed / 1.6;
    }

    set speedUS(value) {
        this.speed = value * 1.6;
    }

    accelerate() {
        this.speed += 10;
        console.log(this.toString())
        return `<p>La voiture <b>${this.brand} ${this.model}</b> avance désormais à <b>${this.speed}km/h</b></p>`
    }

    brake() {
        this.speed = (this.speed - 5) >= 0 ? this.speed - 5 : 0;
        console.log(this.toString())
        return `<p>La voiture <b>${this.brand} ${this.model}</b> avance désormais à <b>${this.speed}km/h</b></p>`
    }

    toString() {
        return `Voiture : ${this.brand} ${this.model} va à ${this.speed}km/h.`;
    }
}

class VoitureElectrique extends Voiture {

    #charge;

    constructor (brand, model, speed, charge) {
        super(brand, model, speed)
        this.#charge = charge;
    }

    chargeBattery(amount) {
        this.#charge = amount > 100 ? 100 : amount;
        console.log(this.toString())
        return `<p>La voiture <b>${this.brand} ${this.model}</b> a sa batterie chargée à <b>${this.#charge}%</b></p>`
    }

    accelerate() {
        this.#charge -= 1;
        return super.accelerate()
    }

    toString() {
        return `${this.brand} ${this.model} va à ${this.speed}km/h et sa batterie est chargée à ${this.#charge}%`;
    }

}

resultOutput.innerHTML += `<h2>Course de Voiture</h2><hr>`

let bmw = new Voiture("BMW", "Serie 1", 80);
let mercedes = new Voiture("Mercedes", "GLB", 100);
let tesla = new VoitureElectrique("Tesla", "Model 3", 70, 25)

resultOutput.innerHTML += bmw.accelerate()
resultOutput.innerHTML += mercedes.brake()
resultOutput.innerHTML += tesla.accelerate()
resultOutput.innerHTML += tesla.brake()
resultOutput.innerHTML += bmw.accelerate()

bmw.speedUS = 80;
mercedes.speedUS = 100
tesla.speedUS = 70;

resultOutput.innerHTML += bmw.accelerate()
resultOutput.innerHTML += mercedes.brake()
resultOutput.innerHTML += mercedes.accelerate()
resultOutput.innerHTML += mercedes.accelerate()
resultOutput.innerHTML += tesla.accelerate()
resultOutput.innerHTML += tesla.accelerate()
resultOutput.innerHTML += "<br>"
while (tesla.speed != 0) resultOutput.innerHTML += tesla.brake()
resultOutput.innerHTML += "<br>"
resultOutput.innerHTML += tesla.chargeBattery(50)
// console.log(tesla.#charge) va lever une erreur car tenter d'accéder à un champ privé