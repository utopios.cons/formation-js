let dateStart = new Date();
let diff = 0;
let chronoRunning = false;
let laps = [new Date(0)]
let monInterval;

const hours = document.querySelector('#hour');
const min = document.querySelector('#min');
const sec = document.querySelector('#sec');
const msec = document.querySelector('#msec');
const lapOutput = document.querySelector('#lap-output');

const btnReset = document.querySelector('#btn-reset');
const btnPausePlay = document.querySelector('#btn-pause-play');
const btnLap = document.querySelector('#btn-lap');

btnPausePlay.addEventListener('click', () => {
    if (!chronoRunning) {
        dateStart = new Date(new Date() - diff);
        monInterval =setInterval(updateChrono, 10);
    } else {
        clearInterval(monInterval)
    }
    chronoRunning = !chronoRunning;
    refreshButtonsStyling();
})

btnLap.addEventListener('click', () => {
    laps.push(diff);
    refreshLapOutput();
})


btnReset.addEventListener('click', () => {
    dateStart = new Date();
    diff = 0;
    chronoRunning = false;
    laps = [new Date(0)]
    refreshChronoValues();
    refreshButtonsStyling();
    refreshLapOutput();
})

const refreshLapOutput = () => {
    lapOutput.innerHTML = '';

    for (let i = 1; i < laps.length; i++) {
        let intermDiff = new Date(laps[i] - laps[i - 1]);
        lapOutput.innerHTML +=
            `<div class="my-2 p-3 row w-75 border border-info rounded align-self-center">
            <div class="col-auto border-end border-info">
                Tour ${i}
            </div>
            <div class="col-auto">
                <b>${(intermDiff.getHours() - 1).toString().padStart(3, '0')} : ${intermDiff.getMinutes().toString().padStart(2, '0')} : ${intermDiff.getSeconds().toString().padStart(2, '0')} : ${intermDiff.getMilliseconds().toString().padStart(3, '0')}</b>
            </div>
        </div>`;
    }
}

const refreshChronoValues = () => {
    diff = new Date(new Date() - dateStart);
    hours.textContent = (diff.getHours() - 1).toString().padStart(2, '0');
    min.textContent = diff.getMinutes().toString().padStart(2, '0');
    sec.textContent = diff.getSeconds().toString().padStart(2, '0');
    msec.textContent = diff.getMilliseconds().toString().padStart(3, '0');
}

const refreshButtonsStyling = () => {
    if (!chronoRunning) {
        btnPausePlay.classList.remove('btn-outline-warning');
        btnPausePlay.classList.add('btn-outline-success');
        btnPausePlay.innerHTML = `<i class="bi bi-play"></i>`;
        btnLap.disabled = true;
    } else {
        btnPausePlay.classList.add('btn-outline-warning');
        btnPausePlay.classList.remove('btn-outline-success');
        btnPausePlay.innerHTML = `<i class="bi bi-pause"></i>`;
        btnLap.disabled = false;
    }
}

const updateChrono = () => {
    if (chronoRunning) refreshChronoValues()
}
