const decrementButton = document.querySelector("#compteur-decrement");
const compteurValue = document.querySelector("#compteur-value");
const incrementButton = document.querySelector("#compteur-increment");

let value = 1;

decrementButton.addEventListener('click', () => {
    if (value > 0) compteurValue.textContent = --value;
    changeCompteurContent();
})

incrementButton.addEventListener('click', () => {
    compteurValue.textContent = ++value;
    changeCompteurContent();
})

const changeCompteurContent = () => {
    if (value % 15 == 0 && value != 0) {
     compteurValue.textContent = "FizzBuzz";
     compteurValue.style.color = "orange";  
    } else if (value % 5 == 0 && value != 0) {
        compteurValue.textContent = "Fizz";
        compteurValue.style.color = "green";
    } else if (value % 3 == 0 && value != 0) {
        compteurValue.textContent = "Buzz";
        compteurValue.style.color = "blue";
    } else {
        compteurValue.style.color = "black";
    }


    if (isNaN(compteurValue.textContent)) compteurValue.classList.add("fizzbuzz");
    else compteurValue.classList.remove("fizzbuzz");
}