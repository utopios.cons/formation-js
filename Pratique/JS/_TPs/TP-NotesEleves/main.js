let addStudentVisibility = false;
let addLessonFieldVisibility = false;
let addGradeVisibility = false;

const addStudentToogleButton = $('#add-student-visibility');
const addLessonFieldToogleButton = $('#add-lessonfield-visibility');
const addGradeToogleButton = $('#add-grade-visibility');

const addStudentForm = $('#add-student-form');
const addLessonFieldForm = $('#add-lessonfield-form');
const addGradeForm = $('#add-grade-form');

const studentChoiceForm = $('#grade-student');
const lessonFieldChoiceForm = $('#grade-field');
const studentChoiceSelect = $('#student-choice');
const lessonFieldChoiceSelect = $('#lessonfield-choice');

const averageGradeOutput = $('#average-grade');

const tableDatas = $('#table-data');

let students = [
    {
        lastName: "MARTIN",
        firstName: "John",
        grades: {
            'français': [12, 14, 9],
            'mathématiques': [8, 7, 12, 15]
        }
    },
    {
        lastName: "DUPONT",
        firstName: "Sophie",
        grades: {
            'français': [18, 17, 15],
            'mathématiques': [4, 5, 12, 15]
        }
    }
]

let lessonFields = ['français', 'mathématiques'];

addStudentForm.submit((data) => {
    data.preventDefault();

    const newStudent = 
    { 
        lastName: $('#student-lastname').val(), 
        firstName: $('#student-firstname').val()
    };

    if (!students.find(x => x.firstName == newStudent.firstName && x.lastName == newStudent.lastName)) 
        students.push(newStudent);

    refreshSelectElements();
});

addLessonFieldForm.submit((data) => {
    data.preventDefault();

    const newLessonField = $('#lesson-field').val().toLowerCase(); 
    if (!lessonFields.includes(newLessonField)) 
        lessonFields.push(newLessonField);

    refreshSelectElements();
});

addGradeForm.submit((data) => {
    data.preventDefault();

    const grade = +$('#grade').val();
    const lessonFieldName = $('#grade-field').val();
    const student = students[(+$('#grade-student').val())-1];

    if (!student.grades) student.grades = {};
    if (!student.grades[lessonFieldName]) student.grades[lessonFieldName] = []; 
        
    student.grades[lessonFieldName].push(grade);

    console.log(students);
});

addStudentToogleButton.click(() => {
    addStudentVisibility = !addStudentVisibility;
    if (addStudentVisibility) {
        addStudentForm.removeClass('d-none');
        addStudentToogleButton.text('ON');
    } else {
        addStudentForm.addClass('d-none');
        addStudentToogleButton.text('OFF');
    }
})

addLessonFieldToogleButton.click(() => {
    addLessonFieldVisibility = !addLessonFieldVisibility;
    if (addLessonFieldVisibility) {
        addLessonFieldForm.removeClass('d-none');
        addLessonFieldToogleButton.text('ON');
    } else {
        addLessonFieldForm.addClass('d-none');
        addLessonFieldToogleButton.text('OFF');
    }
})

addGradeToogleButton.click(() => {
    addGradeVisibility = !addGradeVisibility;
    if (addGradeVisibility) {
        addGradeForm.removeClass('d-none');
        addGradeToogleButton.text('ON');
    } else {
        addGradeForm.addClass('d-none');
        addGradeToogleButton.text('OFF');
    }
})

studentChoiceSelect.change(() => {
    refreshTableElement();
    refreshAverageGrade();
});

lessonFieldChoiceSelect.change(() => {
    refreshTableElement();
    refreshAverageGrade();
});

const capitalize = (value) => {
    value = value.toLowerCase()
    return value.replace(value[0], value[0].toUpperCase());
}

const getStudentFullName = (student) => {
    let firstName = capitalize(student.firstName)
    let lastName = student.lastName.toUpperCase();
    return `${firstName} ${lastName}`;
}

const refreshSelectElements = () => {

    studentChoiceForm.html(`<option value='0'>Sélectionnez un élève</option>`);
    for (const student of students) {
        studentChoiceForm.append(`<option value='${+students.indexOf(student)+1}'>${getStudentFullName(student)}</option>`);
    }

    lessonFieldChoiceForm.html(`<option value='0'>Sélectionnez une matière</option>`);
    for (const field of lessonFields) {
        lessonFieldChoiceForm.append(`<option value='${field}'>${capitalize(field)}</option>`);
    }

    studentChoiceSelect.html(`<option value='0'>Toutes la classe</option>`);
    for (const student of students) {
        studentChoiceSelect.append(`<option value='${+students.indexOf(student)+1}'>${getStudentFullName(student)}</option>`);
    }

    lessonFieldChoiceSelect.html(`<option value='0'>Toutes les matières</option>`);
    for (const field of lessonFields) {
        lessonFieldChoiceSelect.append(`<option value='${field}'>${capitalize(field)}</option>`);
    }
}

const refreshTableElement = () => {
    tableDatas.html("");

    if (+studentChoiceSelect.val() == 0 && +lessonFieldChoiceSelect.val() == 0) {
        for (const student of students) {
            for (const lessonfield in student.grades) {
                for (const grade of student.grades[lessonfield]) {
                    tableDatas.append( 
                    `<tr>
                        <td>${student.lastName}</td>
                        <td>${student.firstName}</td>
                        <td>${lessonfield.replace(lessonfield[0], lessonfield[0].toUpperCase())}</td>
                        <td>${grade}</td>
                    </tr>`);
                }
            }
        }
    } else if (lessonFieldChoiceSelect.val() == 0) {
        const student = students[+studentChoiceSelect.val()-1];

            for (const lessonfield in student.grades) {
                for (const grade of student.grades[lessonfield]) {
                    tableDatas.append( 
                    `<tr>
                        <td>${student.lastName}</td>
                        <td>${student.firstName}</td>
                        <td>${lessonfield.replace(lessonfield[0], lessonfield[0].toUpperCase())}</td>
                        <td>${grade}</td>
                    </tr>`);
                }
            }
    } else if (studentChoiceSelect.val() == 0) {
        const lessonfield = lessonFieldChoiceSelect.val();

        for (const student of students) {
                for (const grade of student.grades[lessonfield]) {
                    tableDatas.append( 
                    `<tr>
                        <td>${student.lastName}</td>
                        <td>${student.firstName}</td>
                        <td>${lessonfield.replace(lessonfield[0], lessonfield[0].toUpperCase())}</td>
                        <td>${grade}</td>
                    </tr>`);
                }
        }
    } else {
        console.log(studentChoiceSelect)
        const student = students[+studentChoiceSelect.val()-1];
        const lessonfield = lessonFieldChoiceSelect.val();

        for (const grade of student.grades[lessonfield]) {
            tableDatas.append( 
            `<tr>
                <td>${student.lastName}</td>
                <td>${student.firstName}</td>
                <td>${lessonfield.replace(lessonfield[0], lessonfield[0].toUpperCase())}</td>
                <td>${grade}</td>
            </tr>`);
        }
    }
   
}

const calculateAverage = (grades) => {
    let sum = 0;
    for (const grade of grades) sum += grade;
    return (sum / grades.length).toFixed(1);
}

const refreshAverageGrade = () => {
    let currentGrades = [];

    if (studentChoiceSelect.val() == 0 && lessonFieldChoiceSelect.val() == 0) {
        for (const student of students) {
            for (const lessonfield in student.grades) {
                for (const grade of student.grades[lessonfield]) {
                    currentGrades.push(grade);
                    averageGradeOutput.html(`Moyenne générale de la classe : <b>${calculateAverage(currentGrades)}</b>`);
                }
            }
        }
    } else if (lessonFieldChoiceSelect.val() == 0) {
        const student = students[studentChoiceSelect.val()-1];
        
        for (const lessonfield in student.grades) {
            for (const grade of student.grades[lessonfield]) {
                currentGrades.push(grade);
                averageGradeOutput.html(`Moyenne générale de <b>${getStudentFullName(student)}</b> : <b>${calculateAverage(currentGrades)}</b>`);
            }
        }
    } else if (studentChoiceSelect.val() == 0) {
        const lessonfield = lessonFieldChoiceSelect.val();
        
        for (const student of students) {
            for (const grade of student.grades[lessonfield]) {
                currentGrades.push(grade);
                averageGradeOutput.html(`Moyenne en <b>${capitalize(lessonfield)}</b> de la classe : <b>${calculateAverage(currentGrades)}</b>`);

            }
        }
    } else {
        const student = students[studentChoiceSelect.val()-1];
        const lessonfield = lessonFieldChoiceSelect.val();
        
        for (const grade of student.grades[lessonfield]) {
            currentGrades.push(grade);
            averageGradeOutput.html(`Moyenne en <b>${capitalize(lessonfield)}</b> de <b>${getStudentFullName(student)}</b> : <b>${calculateAverage(currentGrades)}</b>`);
        }
    }

    console.log(currentGrades);
}

refreshSelectElements();