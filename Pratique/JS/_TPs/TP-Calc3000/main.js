let newInput = true;
let isDotted = false;

const calcScreenDown = document.querySelector('#calc-screen-down');
const calcScreenUp = document.querySelector('#calc-screen-up');
const calcNumbers = document.querySelectorAll(`button[id^='calc-number-'], button[id^='calc-bracket-']`);
const calcOperationButtons = document.querySelectorAll(`button[id^='calc-operation-']`);
const calcEqual = document.querySelector('#calc-equal');
const calcDot = document.querySelector('#calc-dot');
const calcReset = document.querySelector('#calc-reset');

calcReset.addEventListener('click', () => {
    calcScreenUp.textContent = "";
    calcScreenDown.textContent = "";
})

calcNumbers.forEach(x => x.addEventListener('click', (e) => {
    const textValue = e.path
    if (newInput) {
        calcScreenUp.textContent = "";
        calcScreenDown.textContent = textValue;
        newInput = false;
    } else {
        calcScreenDown.textContent += textValue;
    }
}))

calcOperationButtons.forEach(x => x.addEventListener('click', (e) => {
    const textValue = e.path[0].firstChild.textContent;
    if (newInput) {
        calcScreenUp.textContent = calcScreenDown.textContent + textValue;
        calcScreenDown.textContent = "";
        newInput = false;
    } else {
        if (calcScreenUp.textContent) calcScreenUp.textContent += calcScreenDown.textContent + textValue;
        else calcScreenUp.textContent = calcScreenDown.textContent + textValue;
        calcScreenDown.textContent = "";
    }
    isDotted = false;
}))

calcEqual.addEventListener('click', () => {
    calcScreenUp.textContent += calcScreenDown.textContent;
    calcScreenDown.textContent = eval(calcScreenUp.textContent);
    newInput = true;
})

calcDot.addEventListener('click', () => {
    if (!newInput && !isDotted) {
        calcScreenDown.textContent += '.';
        isDotted = true;
    } else if (calcScreenDown.textContent.substring(calcScreenDown.textContent.length - 1, calcScreenDown.textContent.length) === '.') {
        calcScreenDown.textContent = calcScreenDown.textContent.substring(0, calcScreenDown.textContent.length - 1);
        isDotted = false;
    }
})