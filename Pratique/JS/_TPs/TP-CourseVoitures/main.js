class Car {
    static id = 0

    constructor(brand, model, speed) {
        this.id = ++Car.id
        this.brand = brand
        this.model = model
        this.speed = speed
        this.distance = 0
        this.running = true 
    }

    accelerate() {
        this.speed += 10
    }

    brake() {
        this.speed -= 5
    }
}

class ElectricCar extends Car {
    constructor(brand, model, speed) {
        super(brand, model, speed)
        this.electric = true
        this.charge = 100
    }

    accelerate() {
        this.charge -= 1
        super.accelerate()
    }
}

let cars = [
    new Car("BMW", "Serie 3", 90),
    new Car("Renault", "Laguna", 70),
    new ElectricCar("Tesla", "Model 3", 110)
]

const raceFollow = $('#race-follow');
const carChoicesSelect = $('#car-choice');
const accelerateButton = $('#car-accelerate')
const brakeButton = $('#car-brake')
const startButton = $('#car-start');
const stopButton = $('#car-stop');
const refillButton = $('#car-refill');
const carForm = $('#car-form');

carForm.submit((e) => {
    e.preventDefault();

    let newCar
    if ($('#car-electric').attr('checked')) {
        newCar = new ElectricCar(
            $('#car-brand').val(),
            $('#car-model').val(),
            +$('#car-speed').val()
        )
    } else {
        newCar = new Car(
            $('#car-brand').val(),
            $('#car-model').val(),
            +$('#car-speed').val()
        )
    }

    cars.push(newCar);

    refreshCarChoices();
})

carChoicesSelect.change(() => {
    changeControlStates();
})

const changeControlStates = () => {
    const carFound = cars.find(x => x.id == carChoicesSelect.val());
    if (!carFound) {
        $('.car-control').attr("disabled", true)
    } else {
        $('.car-control').removeAttr("disabled", false)
        
    }

    if (carFound.electric && !carFound.running) refillButton.removeAttr("disabled", false)
    else refillButton.attr("disabled", true)

    if (carFound.running) {
        stopButton.removeAttr('disabled')
        startButton.attr('disabled', true)
    } else {
        stopButton.attr('disabled', true)
        startButton.removeAttr('disabled')
    }

    if (carFound.speed <= 0) {
        brakeButton.attr("disabled", true)
    } else {
        brakeButton.removeAttr("disabled")
    }
}

accelerateButton.click(() => {
    const carFound = cars.find(x => x.id == carChoicesSelect.val());
    carFound.accelerate()

    changeControlStates();
})

brakeButton.click(() => {
    const carFound = cars.find(x => x.id == carChoicesSelect.val());
    carFound.brake()

    changeControlStates();
})

$('#car-stop, #car-start').click(() => {
    const carFound = cars.find(x => x.id == carChoicesSelect.val());
    carFound.running = !carFound.running;

    changeControlStates();
})

refillButton.click(() => {
    const carFound = cars.find(x => x.id == carChoicesSelect.val());

    if(carFound.electric && !carFound.running) carFound.charge = 100;
})

const refreshCarChoices = () => {
    carChoicesSelect.html(`<option value="0">Sélectionnez une voiture</option>`);
    for (const car of cars) {
        carChoicesSelect.append(`<option value="${car.id}">${car.brand} ${car.model}</option>`);
    }
}

const refreshCarDistance = () => {
    for (const car of cars) {
        if (car.electric) {
            if (car.charge > 0 && car.running) car.charge--;
            else car.running = false;
        }
        if(car.running) car.distance += car.speed / 3600;
    }

    cars = cars.sort((a, b) => b.distance - a.distance);

    updateCarFollow();
}

const updateCarFollow = () => {
    raceFollow.html("<div class='row'>");

    for (const car of cars) {
        raceFollow.append(
        `
        <div class="col-12 border border-light rounded my-2 card bg-dark">
            <div class="card-header d-flex">
                <span class="card-title me-auto">${car.brand} ${car.model}</span>
                ${car.running ? 
                    "<i class='bi bi-check'></i>" : 
                    "<i class='bi bi-x'></i>"
                }
            </div>
            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item bg-dark text-white">Distance : <b>${car.distance.toFixed(2)}km</b></li>
                    <li class="list-group-item bg-dark text-white">Vitesse : <b>${car.speed}km/h</b></li>
                    ${(car.electric ?
                        `<li class='list-group-item bg-dark text-white'>Charge : <b>${car.charge}%</b></li>` :
                        ""
                    )}
                </ul>
            </div>

            
        </div>
        `);
    }

    raceFollow.append("</div>");
}


refreshCarChoices();
setInterval(refreshCarDistance, 1000);